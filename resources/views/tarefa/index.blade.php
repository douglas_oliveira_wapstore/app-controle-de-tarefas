@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            Tarefas
                        </div>
                        <div class="col-6">
                            <div class="float-right">
                                <a href="{{ route('tarefa.create') }}" class="mr-3">Novo</a>
                                <a href="{{ route('tarefa.exportacao', ['extensao' => 'xlsx']) }}">XLSX</a>
                                <a href="{{ route('tarefa.exportacao', ['extensao' => 'csv']) }}">CSV</a>
                                <a href="{{ route('tarefa.exportacao', ['extensao' => 'pdf']) }}">PDF</a>
                                <a href="{{ route('tarefa.exportar') }}" target="__blank">PDF v2</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Tarefa</th>
                            <th scope="col">Data Limite Conclusão</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($tarefas as $item)
                                <tr>
                                    <th scope="row">{{ $item['id'] }}</th>
                                    <td>{{ $item['tarefa'] }}</td>
                                    <td>{{ date('d/m/Y', strtotime($item['data_limite_conclusao'])) }}</td>
                                    <td><a href="{{ route('tarefa.edit', $item['id']) }}">Editar</a></td>
                                    <td>
                                        <form id="form_{{$item['id']}}" method="post" action="{{ route('tarefa.destroy', ['tarefa' => $item['id']]) }}">
                                            @method('DELETE')
                                            @csrf
                                            <a href="#" onclick="document.getElementById('form_{{$item['id']}}').submit()">Excluir</a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" href="{{ $tarefas->previousPageUrl() }}">Previous</a></li>

                          @for ($i = 1; $i <= $tarefas->lastPage(); $i++)
                            <li class="page-item {{ $tarefas->currentPage() == $i ? 'active' : '' }}">
                                <a class="page-link" href="{{ $tarefas->url($i) }}">{{ $i }}</a>
                            </li>
                          @endfor

                          <li class="page-item"><a class="page-link" href="{{ $tarefas->nextPageUrl() }}">Next</a></li>
                        </ul>
                    </nav>
                    <a href="{{ url()->previous() }}" class="btn btn-primary">Voltar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
