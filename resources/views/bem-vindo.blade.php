Site da aplicação

{{-- Valida se o usuário está autenticado --}}
@auth

    <h1>Usuário Autenticado</h1>

    <p>{{ Auth::user()->id }}</p>
    <p>{{ Auth::user()->name }}</p>
    <p>{{ Auth::user()->email }}</p>

@endauth

{{-- Valida se o usuário é visitante --}}
@guest
    Olá Visitante, tudo bem?
@endguest